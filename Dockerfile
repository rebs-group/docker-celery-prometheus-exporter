FROM python:alpine

MAINTAINER Felix Kerekes <felix@rebs-group.com>

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY entrypoint.sh .

CMD "./entrypoint.sh"
